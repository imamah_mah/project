from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('koperasi/', include('koperasi.urls')),
    path('pembayaran/', include('pembayaran.urls')),
    path('pinjaman/', include('pinjaman.urls')),
    
]
